require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.any? ? arr.reduce(:+) : 0
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |word| word.include?(substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  ('a'..'z').select { |el| string.count(el) > 1 }
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.delete!(",.;:!?")
  string.split.sort_by{|w| w.length}[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ('a'..'z').select { |c| !string.include?(c) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |yr| not_repeat_year?(yr) }
end

def not_repeat_year?(year)
  (0..9).none? { |c| year.to_s.count(c.to_s) > 1 }
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.each_with_index { |s,i| songs.delete(s) if s == songs[i+1] }.uniq
end

def no_repeats?(song_name, songs)
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
    string.delete!(",.;:!?")
    string.split.reduce(nil) do |a,el|
        if c_distance(el).nil?
            a
        elsif a.nil?
            a = el
        elsif c_distance(el) < c_distance(a)
            a = el
        else
            a
        end
    end

end

def c_distance(word)
    if !word.nil?
        word.reverse.chars.each_with_index { |char,i| return i if char == 'c' }
    end
    nil
end


# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
    result = []
    slice = []
    arr.each_with_index do |n,i|
        if n == arr[i+1]
            slice << i
        elsif n != arr[i+1]
            slice << i
            if slice.length > 1
                result << [slice.first, slice.last]
            end
            slice = []
        end
    end
    result
end
